package zhongjyuan.wish.batis.generate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.fill.Column;
import com.baomidou.mybatisplus.generator.fill.Property;

import zhongjyuan.domain.Constant;

/**
 * @ClassName CustomAutoGenerator
 * @Description 自定义自动生成器
 * @Author zhongjyuan
 * @Date 2023年1月5日 下午2:14:12
 * @Copyright zhongjyuan.com
 */
public class CustomAutoGenerator {

	/**
	 * @Title initialize
	 * @Author zhongjyuan
	 * @Description 初始化
	 * @Param
	 * @Return void
	 * @Throws
	 */
	public void initialize() {
		datasource_url = "jdbc:mysql://172.56.120.16:3306/wish_dev?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=GMT%2B8";
		datasource_userName = "wish_dev";
		datasource_password = "haLxawKMTiSkGWj4";

		global_author = "zhongjyuan";
		global_commentDate = "yyyy年MM月dd日 HH:mm:ss";
		global_outputDir = System.getProperty("user.dir") + "/src/main/java";

		template_xml = "templates/mapper.xml.java";
		template_entity = "templates/entity.java";
		template_mapper = "templates/mapper.java";
		template_service = "templates/service.java";
		template_serviceImpl = "templates/serviceImpl.java";
		template_controller = "templates/controller.java";

		package_parent = "zhongjyuan.wish";
		package_moduleName = "system";

		package_xml = "mapper.xml";
		package_entity = "model.entity";
		package_mapper = "mapper";
		package_service = "service";
		package_serviceImpl = "service.impl";
		package_controller = "controller";

		strategy_tablePrefix = Arrays.asList();
		strategy_includeTable = Arrays.asList();

		injection_copyright = "Copyright (c) " + LocalDate.now().getYear() + " zhongjyuan.com";

		injection_package_model = "model";
		injection_package_vo_request = "model.vo.request";
		injection_package_vo_response = "model.vo.response";
		injection_package_query = "model.query";
		injection_package_converter = "model.converter";

		injection_template_createvo = "templates/createVO.java";
		injection_template_updatevo = "templates/updateVO.java";
		injection_template_vo = "templates/vo.java";
		injection_template_pagevo = "templates/pageVO.java";
		injection_template_briefvo = "templates/briefVO.java";
		injection_template_treevo = "templates/treeVO.java";
		injection_template_query = "templates/query.java";
		injection_template_pagequery = "templates/pageQuery.java";
		injection_template_briefquery = "templates/briefQuery.java";
		injection_template_lazyquery = "templates/lazyQuery.java";
		injection_template_treequery = "templates/treeQuery.java";
		injection_template_complexquery = "templates/complexQuery.java";
		injection_template_responsecode = "templates/responseCode.java";
		injection_template_converter = "templates/converter.java";

		// CreateVO忽略属性
		injection_createvo_ignorePropertyName = new ArrayList<String>();
		injection_createvo_ignorePropertyName.add(Constant.ID_L);
		injection_createvo_ignorePropertyName.add(Constant.LEVEL_L);
		injection_createvo_ignorePropertyName.add(Constant.PATH_L);
		injection_createvo_ignorePropertyName.add(Constant.OUTLINE_L);
		injection_createvo_ignorePropertyName.add(Constant.IS_LEAF_L_A);
		injection_createvo_ignorePropertyName.add(Constant.IS_LOCK_L_A);
		injection_createvo_ignorePropertyName.add(Constant.IS_SYSTEM_L_A);
		injection_createvo_ignorePropertyName.add(Constant.IS_ENABLED_L_A);
		injection_createvo_ignorePropertyName.add(Constant.IS_DELETED_L_A);
		injection_createvo_ignorePropertyName.add(Constant.CREATOR_ID_L_A);
		injection_createvo_ignorePropertyName.add(Constant.CREATOR_NAME_L_A);
		injection_createvo_ignorePropertyName.add(Constant.CREATE_TIME_L_A);
		injection_createvo_ignorePropertyName.add(Constant.UPDATOR_ID_L_A);
		injection_createvo_ignorePropertyName.add(Constant.UPDATOR_NAME_L_A);
		injection_createvo_ignorePropertyName.add(Constant.UPDATE_TIME_L_A);

		// UpdateVO忽略属性
		injection_updatevo_ignorePropertyName = new ArrayList<String>();
		injection_updatevo_ignorePropertyName.add(Constant.LEVEL_L);
		injection_updatevo_ignorePropertyName.add(Constant.PATH_L);
		injection_updatevo_ignorePropertyName.add(Constant.OUTLINE_L);
		injection_updatevo_ignorePropertyName.add(Constant.IS_LEAF_L_A);
		injection_updatevo_ignorePropertyName.add(Constant.IS_LOCK_L_A);
		injection_updatevo_ignorePropertyName.add(Constant.IS_SYSTEM_L_A);
		injection_updatevo_ignorePropertyName.add(Constant.IS_ENABLED_L_A);
		injection_updatevo_ignorePropertyName.add(Constant.IS_DELETED_L_A);
		injection_updatevo_ignorePropertyName.add(Constant.CREATOR_ID_L_A);
		injection_updatevo_ignorePropertyName.add(Constant.CREATOR_NAME_L_A);
		injection_updatevo_ignorePropertyName.add(Constant.CREATE_TIME_L_A);
		injection_updatevo_ignorePropertyName.add(Constant.UPDATOR_ID_L_A);
		injection_updatevo_ignorePropertyName.add(Constant.UPDATOR_NAME_L_A);
		injection_updatevo_ignorePropertyName.add(Constant.UPDATE_TIME_L_A);

		// VO忽略属性
		injection_vo_ignorePropertyName = new ArrayList<String>();
		injection_vo_ignorePropertyName.add(Constant.PATH_L);
		injection_vo_ignorePropertyName.add(Constant.OUTLINE_L);
		injection_vo_ignorePropertyName.add(Constant.IS_LOCK_L_A);
		injection_vo_ignorePropertyName.add(Constant.IS_DELETED_L_A);

		// BriefVO忽略属性
		injection_briefvo_ignorePropertyName = new ArrayList<String>();
		injection_briefvo_ignorePropertyName.add(Constant.LEVEL_L);
		injection_briefvo_ignorePropertyName.add(Constant.PATH_L);
		injection_briefvo_ignorePropertyName.add(Constant.OUTLINE_L);
		injection_briefvo_ignorePropertyName.add(Constant.IS_LEAF_L_A);
		injection_briefvo_ignorePropertyName.add(Constant.IS_LOCK_L_A);
		injection_briefvo_ignorePropertyName.add(Constant.IS_SYSTEM_L_A);
		injection_briefvo_ignorePropertyName.add(Constant.IS_ENABLED_L_A);
		injection_briefvo_ignorePropertyName.add(Constant.IS_DELETED_L_A);
		injection_briefvo_ignorePropertyName.add(Constant.CREATOR_ID_L_A);
		injection_briefvo_ignorePropertyName.add(Constant.CREATOR_NAME_L_A);
		injection_briefvo_ignorePropertyName.add(Constant.CREATE_TIME_L_A);
		injection_briefvo_ignorePropertyName.add(Constant.UPDATOR_ID_L_A);
		injection_briefvo_ignorePropertyName.add(Constant.UPDATOR_NAME_L_A);
		injection_briefvo_ignorePropertyName.add(Constant.UPDATE_TIME_L_A);

		// PageVO忽略属性
		injection_pagevo_ignorePropertyName = new ArrayList<String>();
		injection_pagevo_ignorePropertyName.add(Constant.PATH_L);
		injection_pagevo_ignorePropertyName.add(Constant.OUTLINE_L);
		injection_pagevo_ignorePropertyName.add(Constant.IS_LOCK_L_A);
		injection_pagevo_ignorePropertyName.add(Constant.IS_DELETED_L_A);

		// TreeVO忽略属性
		injection_treevo_ignorePropertyName = new ArrayList<String>();
		injection_treevo_ignorePropertyName.add(Constant.PATH_L);
		injection_treevo_ignorePropertyName.add(Constant.OUTLINE_L);
		injection_treevo_ignorePropertyName.add(Constant.IS_LEAF_L_A);
		injection_treevo_ignorePropertyName.add(Constant.IS_LOCK_L_A);
		injection_treevo_ignorePropertyName.add(Constant.IS_DELETED_L_A);

		// Query忽略属性
		injection_query_ignorePropertyName = new ArrayList<String>();
		injection_query_ignorePropertyName.add(Constant.ID_L);
		injection_query_ignorePropertyName.add(Constant.NO_L);
		injection_query_ignorePropertyName.add(Constant.CODE_L);
		injection_query_ignorePropertyName.add(Constant.NAME_L);
		injection_query_ignorePropertyName.add(Constant.LEVEL_L);
		injection_query_ignorePropertyName.add(Constant.PATH_L);
		injection_query_ignorePropertyName.add(Constant.OUTLINE_L);
		injection_query_ignorePropertyName.add(Constant.IS_LEAF_L_A);
		injection_query_ignorePropertyName.add(Constant.IS_LOCK_L_A);
		injection_query_ignorePropertyName.add(Constant.IS_DELETED_L_A);
		injection_query_ignorePropertyName.add(Constant.ROW_INDEX_L_A);
		injection_query_ignorePropertyName.add(Constant.DESCIPTION_L);
		injection_query_ignorePropertyName.add(Constant.CREATOR_ID_L_A);
		injection_query_ignorePropertyName.add(Constant.CREATOR_NAME_L_A);
		injection_query_ignorePropertyName.add(Constant.CREATE_TIME_L_A);
		injection_query_ignorePropertyName.add(Constant.UPDATOR_ID_L_A);
		injection_query_ignorePropertyName.add(Constant.UPDATOR_NAME_L_A);
		injection_query_ignorePropertyName.add(Constant.UPDATE_TIME_L_A);

		// BriefQuery忽略属性
		injection_briefquery_ignorePropertyName = new ArrayList<String>();
		injection_briefquery_ignorePropertyName.add(Constant.ID_L);
		injection_briefquery_ignorePropertyName.add(Constant.NO_L);
		injection_briefquery_ignorePropertyName.add(Constant.CODE_L);
		injection_briefquery_ignorePropertyName.add(Constant.NAME_L);
		injection_briefquery_ignorePropertyName.add(Constant.LEVEL_L);
		injection_briefquery_ignorePropertyName.add(Constant.PATH_L);
		injection_briefquery_ignorePropertyName.add(Constant.OUTLINE_L);
		injection_briefquery_ignorePropertyName.add(Constant.IS_LEAF_L_A);
		injection_briefquery_ignorePropertyName.add(Constant.IS_LOCK_L_A);
		injection_briefquery_ignorePropertyName.add(Constant.IS_DELETED_L_A);
		injection_briefquery_ignorePropertyName.add(Constant.ROW_INDEX_L_A);
		injection_briefquery_ignorePropertyName.add(Constant.DESCIPTION_L);
		injection_briefquery_ignorePropertyName.add(Constant.CREATOR_ID_L_A);
		injection_briefquery_ignorePropertyName.add(Constant.CREATOR_NAME_L_A);
		injection_briefquery_ignorePropertyName.add(Constant.CREATE_TIME_L_A);
		injection_briefquery_ignorePropertyName.add(Constant.UPDATOR_ID_L_A);
		injection_briefquery_ignorePropertyName.add(Constant.UPDATOR_NAME_L_A);
		injection_briefquery_ignorePropertyName.add(Constant.UPDATE_TIME_L_A);

		// PageQuery忽略属性
		injection_pagequery_ignorePropertyName = new ArrayList<String>();
		injection_pagequery_ignorePropertyName.add(Constant.ID_L);
		injection_pagequery_ignorePropertyName.add(Constant.NO_L);
		injection_pagequery_ignorePropertyName.add(Constant.CODE_L);
		injection_pagequery_ignorePropertyName.add(Constant.NAME_L);
		injection_pagequery_ignorePropertyName.add(Constant.LEVEL_L);
		injection_pagequery_ignorePropertyName.add(Constant.PATH_L);
		injection_pagequery_ignorePropertyName.add(Constant.OUTLINE_L);
		injection_pagequery_ignorePropertyName.add(Constant.IS_LEAF_L_A);
		injection_pagequery_ignorePropertyName.add(Constant.IS_LOCK_L_A);
		injection_pagequery_ignorePropertyName.add(Constant.IS_DELETED_L_A);
		injection_pagequery_ignorePropertyName.add(Constant.ROW_INDEX_L_A);
		injection_pagequery_ignorePropertyName.add(Constant.DESCIPTION_L);
		injection_pagequery_ignorePropertyName.add(Constant.CREATOR_ID_L_A);
		injection_pagequery_ignorePropertyName.add(Constant.CREATOR_NAME_L_A);
		injection_pagequery_ignorePropertyName.add(Constant.CREATE_TIME_L_A);
		injection_pagequery_ignorePropertyName.add(Constant.UPDATOR_ID_L_A);
		injection_pagequery_ignorePropertyName.add(Constant.UPDATOR_NAME_L_A);
		injection_pagequery_ignorePropertyName.add(Constant.UPDATE_TIME_L_A);
	}

	/**
	 * @Title excute
	 * @Author zhongjyuan
	 * @Description 执行
	 * @Param
	 * @Return void
	 * @Throws
	 */
	public void excute() {
		excute(null, null);
	}

	/**
	 * @Title excute
	 * @Author zhongjyuan
	 * @Description 执行
	 * @Param @param includeTable 包含表
	 * @Return void
	 * @Throws
	 */
	public void excute(List<String> includeTable) {
		excute(null, includeTable);
	}

	/**
	 * @Title excute
	 * @Author zhongjyuan
	 * @Description 执行
	 * @Param @param tablePrefix 表前缀
	 * @Param @param includeTable 包含表
	 * @Return void
	 * @Throws
	 */
	@SuppressWarnings("deprecation")
	public void excute(List<String> tablePrefix, List<String> includeTable) {
		// 初始化
		initialize();

		// 实例化FastAutoGenerator
		FastAutoGenerator.create(datasource_url, datasource_userName, datasource_password) // 构建DataSourceConfig
				// 构建GlobalConfig
				.globalConfig(builder -> {
					builder.author(global_author) // 设置作者
							.commentDate(global_commentDate) // 设置注释日期格式
							.dateType(DateType.TIME_PACK) // 设置实体类日期类型
							.outputDir(global_outputDir) // 设置输出目录
							.enableSwagger() // 开启Swagger模式
							.disableOpenDir() // 禁止打开输出目录
							.fileOverride() // 覆盖已存在文件
					// 构建GlobalConfig结尾
					;
				})
				// 构建PackageConfig
				.packageConfig(builder -> {
					builder.parent(package_parent) // 设置父级包名
							.moduleName(package_moduleName) // 设置模块名
							.entity(package_entity) // 设置Entity包名
							.mapper(package_mapper) // 设置Mapper接口包名
							.xml(package_xml) // 设置Mapper XML包名
							.service(package_service) // 设置Service接口包名
							.serviceImpl(package_serviceImpl) // 设置Service接口实现包名
							.controller(package_controller) // 设置Controller包名
					// 构建PackageConfig结尾
					;
				})
				// 构建TemplateConfig
				.templateConfig(builder -> {
					builder.entity(template_entity) // 设置entity模板
							.mapper(template_mapper) // 设置Mapper接口模板
							.xml(template_xml) // 设置Mapper xml模板
							.service(template_service) // 设置service接口模板
							.serviceImpl(template_serviceImpl) // 设置service接口实现模板
							.controller(template_controller) // 设置controller模板
					// 构建TemplateConfig结尾
					;
				})
				// 构建StrategyConfig
				.strategyConfig(builder -> {
					builder.addTablePrefix(tablePrefix == null ? strategy_tablePrefix : tablePrefix) // 设置过滤表前缀
							.addTableSuffix(strategy_tableSuffix) // 设置过滤表后缀
							.addFieldPrefix(strategy_fieldPrefix) // 设置过滤字段前缀
							.addFieldSuffix(strategy_fieldSuffix) // 设置过滤字段后缀
							.addInclude(includeTable == null ? strategy_includeTable : includeTable) // 设置包含表名
							//.addExclude(strategy_excludeTable) // 设置过滤表名

							// 设置Entity策略
							.entityBuilder() // 开启EntityBuilder
							.idType(IdType.ASSIGN_ID) // 设置主键策略
							.formatFileName("%sDO") // 设置Entity文件名格式
							.addTableFills(new Column("tenant_id", FieldFill.INSERT)) // 增加表字段填充 
							.addTableFills(new Property("tenant_code", FieldFill.INSERT_UPDATE)) // 增加属性填充
							.addIgnoreColumns(Arrays.asList("")) // 设置忽略字段
							.addSuperEntityColumns(Arrays.asList("")) // 设置父类公共字段
							.logicDeleteColumnName("is_deleted") // 设置逻辑删除字段名称
							.logicDeletePropertyName("isDeleted") // 逻辑删除属性名称
							.versionColumnName("version") // 设置乐观锁字段名称
							.versionPropertyName("version") // 设置乐观锁属性名称
							.naming(NamingStrategy.underline_to_camel) // 设置数据库表字段映射到实体的命名策略
							.columnNaming(NamingStrategy.underline_to_camel) // 设置数据库表字段映射到实体的命名策略
							.enableTableFieldAnnotation() // 开启实体字段注释
							//.enableRemoveIsPrefix() // 开启Boolean类型字段移除is前缀
							//.disableSerialVersionUID() // 禁用生成serialVersionUID
							.enableLombok() // 开启lombok模式
							.enableChainModel() // 开启链式模式
							.enableColumnConstant() // 开启生成字段常量
							.fileOverride() // 开启覆盖已有文件

							// 设置Mapper策略
							.mapperBuilder() // 开启MapperBuilder
							.formatXmlFileName("%sMapper") // 设置Mapper XML文件名格式
							.formatMapperFileName("%sMapper") // 设置Mapper接口文件名格式
							.enableMapperAnnotation() // 开启 @Mapper 注解
							.enableBaseResultMap() // 开启baseResultMap
							.enableBaseColumnList() // 开启baseColumnList
							.fileOverride() // 开启覆盖已有文件

							// 设置Service策略
							.serviceBuilder() // 开启ServiceBuilder
							.formatServiceFileName("I%sService") // 设置Service接口文件名格式
							.formatServiceImplFileName("%sServiceImpl") // 设置Service接口实现文件名格式
							//.superServiceClass("") // 设置Service接口父级类
							//.superServiceImplClass("") // 设置Service接口实现父级类
							.fileOverride() // 开启覆盖已有文件

							// 设置Controller策略
							.controllerBuilder() // 开启ControllerBuilder
							.formatFileName("%sController") // 设置Controller文件名格式
							//.superClass("") // 设置Controller父级类
							.enableRestStyle() // 开启生成@RestController控制器
							.fileOverride() // 开启覆盖已有文件
					// 构建StrategyConfig结尾
					;
				})
				// 构建InjectionConfig
				.injectionConfig(builder -> {
					builder.customMap(new HashMap<String, Object>() {

						private static final long serialVersionUID = 1L;
						{
							this.put("copyright", injection_copyright);

							this.put("import_Model", injection_import_domain);

							this.put("import_Model_AbstractModel", injection_import_domain_abstractmodel);
							this.put("import_Model_AbstractEntity", injection_import_domain_abstractentity);

							this.put("import_Model_LogType", injection_import_domain_logtype);
							this.put("import_Model_Query", injection_import_domain_query);
							this.put("import_Model_SortQuery", injection_import_domain_sortquery);
							this.put("import_Model_PageQuery", injection_import_domain_pagequery);
							this.put("import_Model_BatchQuery", injection_import_domain_batchquery);
							this.put("import_Model_TreeQuery", injection_import_domain_treequery);
							this.put("import_Model_TreeLazyQuery", injection_import_domain_lazyquery);
							this.put("import_Model_TreeComplexQuery", injection_import_domain_complexquery);

							this.put("import_Model_Constant", injection_import_domain_constant);
							this.put("import_Model_PageResult", injection_import_domain_pageresult);

							this.put("import_Commons_Log", injection_import_commons_log);
							this.put("import_Commons_ResponseResult", injection_import_domain_responseresult);
							this.put("import_Commons_ApiVersion", injection_import_commons_apiversion);

							this.put("import_Commons_BeanUtil", injection_import_domain_beanutil);
							this.put("import_Commons_EntityUtil", injection_import_domain_entityutil);
							this.put("import_Commons_BusinessException", injection_import_domain_businessexception);
							this.put("import_Commons_GlobalConfigurate", injection_import_commons_globalconfigurate);

							this.put("import_Manage_TreeManage", injection_import_manage_treemanage);

							this.put("import_Batis_PageUtil", injection_import_batis_pageutil);
							this.put("import_Batis_CustomPage", injection_import_batis_custompage);

							this.put("package_Model", injection_package_model);
							this.put("package_RequestVO", injection_package_vo_request);
							this.put("package_ResponseVO", injection_package_vo_response);
							this.put("package_Query", injection_package_query);
							this.put("package_Converter", injection_package_converter);

							this.put("createvo_ignorePropertyName", injection_createvo_ignorePropertyName);
							this.put("updatevo_ignorePropertyName", injection_updatevo_ignorePropertyName);

							this.put("vo_ignorePropertyName", injection_vo_ignorePropertyName);
							this.put("briefvo_ignorePropertyName", injection_briefvo_ignorePropertyName);
							this.put("pagevo_ignorePropertyName", injection_pagevo_ignorePropertyName);
							this.put("treevo_ignorePropertyName", injection_treevo_ignorePropertyName);

							this.put("query_ignorePropertyName", injection_query_ignorePropertyName);
							this.put("briefquery_ignorePropertyName", injection_briefquery_ignorePropertyName);
							this.put("pagequery_ignorePropertyName", injection_pagequery_ignorePropertyName);
							this.put("treequery_ignorePropertyName", injection_treequery_ignorePropertyName);
							this.put("lazyquery_ignorePropertyName", injection_lazyquery_ignorePropertyName);
							this.put("complexquery_ignorePropertyName", injection_complexquery_ignorePropertyName);
						}
					}) // 设置自定义配置 Map 对象
							.customFile(new HashMap<String, String>() {

								private static final long serialVersionUID = 1L;
								{
									this.put("%sCreateVO", injection_template_createvo);
									this.put("%sUpdateVO", injection_template_updatevo);

									this.put("%sVO", injection_template_vo);
									this.put("%sPageVO", injection_template_pagevo);
									this.put("%sBriefVO", injection_template_briefvo);

									this.put("%sTreeVO", injection_template_treevo);

									this.put("%sQuery", injection_template_query);
									this.put("%sPageQuery", injection_template_pagequery);
									this.put("%sBriefQuery", injection_template_briefquery);

									//this.put("%sTreeLazyQuery", injection_template_lazyquery);
									//this.put("%sTreeQuery", injection_template_treequery);
									//this.put("%sTreeComplexQuery", injection_template_complexquery);

									this.put("%sCode", injection_template_responsecode);
									this.put("%sConverter", injection_template_converter);

									this.put("proc_%s_update_outline.sql", injection_template_proc_update_outline);
									this.put("proc_%s_parent_nodes.sql", injection_template_proc_parent_nodes);
									this.put("proc_%s_children_nodes.sql", injection_template_proc_children_nodes);
									this.put("proc_%s_leaf_nodes.sql", injection_template_proc_leaf_nodes);
									this.put("proc_%s_full_nodes.sql", injection_template_proc_full_nodes);
								}
							}) // 设置自定义配置模板文件
							.fileOverride() // 开启覆盖已有文件
					// 构建InjectionConfig结尾
					;
				})
				// 设置模板引擎
				.templateEngine(new CustomFreemarkerTemplateEngine())
				// 开始执行
				.execute();
		// 结尾
		;
	}

	/**
	 * @Fields datasource_url: 驱动连接的URL
	 */
	protected String datasource_url;

	/**
	 * @Fields datasource_userName: 数据库连接用户名
	 */
	protected String datasource_userName;

	/**
	 * @Fields datasource_password: 数据库连接密码
	 */
	protected String datasource_password;

	/**
	 * @Fields datasource_schemaName: schemaName
	 */
	protected String datasource_schemaName;

	/**
	 * @Fields global_author: 作者
	 */
	protected String global_author = "zhongjyuan";

	/**
	 * @Fields global_outputDir: 生成文件的输出目录
	 */
	protected String global_outputDir = System.getProperty("user.dir") + "/src/main/java";

	/**
	 * @Fields global_commentDate: 指定注释日期格式化
	 */
	protected String global_commentDate = "yyyy年MM月dd日 HH:mm:ss";

	/**
	 * @Fields template_xml: MapperXml模板路径
	 */
	protected String template_xml = "templates/mapper.xml.java";

	/**
	 * @Fields template_entity: 实体模板路径
	 */
	protected String template_entity = "templates/entity.java";

	/**
	 * @Fields template_mapper: Mapper模板路径
	 */
	protected String template_mapper = "templates/mapper.java";

	/**
	 * @Fields template_service: Service模板路径
	 */
	protected String template_service = "templates/service.java";

	/**
	 * @Fields template_serviceImpl: ServiceImpl模板路径
	 */
	protected String template_serviceImpl = "templates/serviceImpl.java";

	/**
	 * @Fields template_controller: 控制器模板路径
	 */
	protected String template_controller = "templates/controller.java";

	/**
	 * @Fields package_parent: 父包名
	 */
	protected String package_parent = "com.wish";

	/**
	 * @Fields package_moduleName: 父包模块名
	 */
	protected String package_moduleName = "system";

	/**
	 * @Fields package_xml: Mapper XML包名
	 */
	protected String package_xml = "mapper.xml";

	/**
	 * @Fields package_entity: Entity包名
	 */
	protected String package_entity = "model.entity";

	/**
	 * @Fields package_mapper: Mapper包名
	 */
	protected String package_mapper = "mapper";

	/**
	 * @Fields package_service: Service包名
	 */
	protected String package_service = "service";

	/**
	 * @Fields package_serviceImpl: Service Impl包名
	 */
	protected String package_serviceImpl = "service.impl";

	/**
	 * @Fields package_controller: Controller包名
	 */
	protected String package_controller = "controller";

	/**
	 * @Fields strategy_tablePrefix: 过滤表前缀
	 */
	public List<String> strategy_tablePrefix = Arrays.asList("sys_"); // 过滤表前缀

	/**
	 * @Fields strategy_tableSuffix: 过滤表后缀
	 */
	public List<String> strategy_tableSuffix = Arrays.asList(""); // 过滤表后缀

	/**
	 * @Fields strategy_fieldPrefix: 过滤字段前缀
	 */
	public List<String> strategy_fieldPrefix = Arrays.asList(""); // 过滤字段前缀

	/**
	 * @Fields strategy_fieldSuffix: 过滤字段后缀
	 */
	public List<String> strategy_fieldSuffix = Arrays.asList(""); // 过滤字段后缀

	/**
	 * @Fields strategy_includeTable: 需要包含的表名
	 */
	public List<String> strategy_includeTable = Arrays.asList(""); // 需要包含的表名

	/**
	 * @Fields strategy_excludeTable: 需要排除的表名
	 */
	public List<String> strategy_excludeTable = Arrays.asList(""); // 需要排除的表名

	/**
	 * @Fields injection_copyright: 注入版权
	 */
	protected String injection_copyright = "www.zhongjyuan.com";

	protected String injection_import_domain = "zhongjyuan.domain.*";

	protected String injection_import_domain_abstractmodel = "zhongjyuan.domain.AbstractModel";

	protected String injection_import_domain_abstractentity = "zhongjyuan.domain.AbstractEmptyEntity";

	protected String injection_import_domain_logtype = "zhongjyuan.domain.LogType";

	protected String injection_import_domain_query = "zhongjyuan.domain.query.*";

	protected String injection_import_domain_sortquery = "zhongjyuan.domain.query.SortQuery";

	protected String injection_import_domain_pagequery = "zhongjyuan.domain.query.PageQuery";

	protected String injection_import_domain_batchquery = "zhongjyuan.domain.query.BatchQuery";

	protected String injection_import_domain_treequery = "zhongjyuan.domain.query.TreeQuery";

	protected String injection_import_domain_lazyquery = "zhongjyuan.domain.query.TreeLazyQuery";

	protected String injection_import_domain_complexquery = "zhongjyuan.domain.query.TreeComplexQuery";

	protected String injection_import_domain_constant = "zhongjyuan.domain.Constant";

	protected String injection_import_domain_pageresult = "zhongjyuan.domain.response.PageResult";

	protected String injection_import_domain_responseresult = "zhongjyuan.domain.response.ResponseResult";

	protected String injection_import_domain_beanutil = "zhongjyuan.domain.utils.BeanUtil";

	protected String injection_import_domain_entityutil = "zhongjyuan.domain.utils.EntityUtil";

	protected String injection_import_domain_businessexception = "zhongjyuan.domain.exception.BusinessException";

	protected String injection_import_manage_treemanage = "zhongjyuan.wish.plugin.framework.TreeManage";

	protected String injection_import_batis_pageutil = "zhongjyuan.wish.batis.utils.PageUtil";

	protected String injection_import_batis_custompage = "zhongjyuan.wish.batis.pagehelper.CustomPage";

	protected String injection_import_commons_log = "zhongjyuan.wish.annotation.Log";

	protected String injection_import_commons_apiversion = "zhongjyuan.wish.annotation.ApiVersion";

	protected String injection_import_commons_globalconfigurate = "zhongjyuan.wish.configurate.global.GlobalConfigurate";

	/**
	 * @Fields injection_createvo_ignorePropertyName: 注入CreateVO忽略属性名集合
	 */
	public List<String> injection_createvo_ignorePropertyName = Arrays.asList("id", "level", "path", "outline", "isLeaf", "isEnabled", "isDeleted", "creatorId", "creatorName", "createTime", "updatorId", "updatorName", "updateTime");

	/**
	 * @Fields injection_updatevo_ignorePropertyName: 注入UpdateVO忽略属性名集合
	 */
	public List<String> injection_updatevo_ignorePropertyName = Arrays.asList("level", "path", "outline", "isLeaf", "isSystem", "isEnabled", "isDeleted", "creatorId", "creatorName", "createTime", "updatorId", "updatorName", "updateTime");

	/**
	 * @Fields injection_vo_ignorePropertyName: 注入VO忽略属性名集合
	 */
	public List<String> injection_vo_ignorePropertyName = Arrays.asList("path", "outline", "isDeleted");

	/**
	 * @Fields injection_briefvo_ignorePropertyName: 注入BriefVO忽略属性名集合
	 */
	public List<String> injection_briefvo_ignorePropertyName = Arrays.asList("level", "path", "outline", "isLeaf", "isSystem", "isEnabled", "isDeleted", "creatorId", "creatorName", "createTime", "updatorId", "updatorName", "updateTime");

	/**
	 * @Fields injection_pagevo_ignorePropertyName: 注入PageVO忽略属性名集合
	 */
	public List<String> injection_pagevo_ignorePropertyName = Arrays.asList("path", "outline", "isDeleted");

	/**
	 * @Fields injection_treevo_ignorePropertyName: 注入TreeVO忽略属性名集合
	 */
	public List<String> injection_treevo_ignorePropertyName = Arrays.asList("path", "outline", "isDeleted");

	/**
	 * @Fields injection_query_ignorePropertyName: 注入Query忽略属性名集合
	 */
	public List<String> injection_query_ignorePropertyName = Arrays.asList("id", "no", "code", "name", "level", "path", "outline", "isLeaf", "isDeleted", "rowIndex", "description", "creatorId", "creatorName", "createTime", "updatorId", "updatorName", "updateTime");

	/**
	 * @Fields injection_briefquery_ignorePropertyName: 注入BriefQuery忽略属性名集合
	 */
	public List<String> injection_briefquery_ignorePropertyName = Arrays.asList("id", "no", "code", "name", "level", "path", "outline", "isLeaf", "isDeleted", "rowIndex", "description", "creatorId", "creatorName", "createTime", "updatorId", "updatorName", "updateTime");

	/**
	 * @Fields injection_pagequery_ignorePropertyName: 注入PageQuery忽略属性名集合
	 */
	public List<String> injection_pagequery_ignorePropertyName = Arrays.asList("id", "no", "code", "name", "level", "path", "outline", "isLeaf", "isDeleted", "rowIndex", "description", "creatorId", "creatorName", "createTime", "updatorId", "updatorName", "updateTime");

	/**
	 * @Fields injection_treequery_ignorePropertyName: 注入TreeQuery忽略属性名集合
	 */
	public List<String> injection_treequery_ignorePropertyName = Arrays.asList("");

	/**
	 * @Fields injection_lazyquery_ignorePropertyName: 注入TreeLazyQuery忽略属性名集合
	 */
	public List<String> injection_lazyquery_ignorePropertyName = Arrays.asList("");

	/**
	 * @Fields injection_complexquery_ignorePropertyName: 注入TreeComplexQuery忽略属性名集合
	 */
	public List<String> injection_complexquery_ignorePropertyName = Arrays.asList("");

	/**
	 * @Fields injection_package_model: 注入模型包名
	 */
	protected String injection_package_model = "model";

	/**
	 * @Fields injection_package_vo_request: 注入请求VO包名
	 */
	protected String injection_package_vo_request = "model.vo.request";

	/**
	 * @Fields injection_package_vo_response: 注入响应VO包名
	 */
	protected String injection_package_vo_response = "model.vo.response";

	/**
	 * @Fields injection_package_query: 注入Query包名
	 */
	protected String injection_package_query = "model.query";

	/**
	 * @Fields injection_package_converter: 注入Converter包名
	 */
	protected String injection_package_converter = "model.converter";

	/**
	 * @Fields injection_template_createvo: 注入CreateVO模板
	 */
	protected String injection_template_createvo = "templates/createVO.java";

	/**
	 * @Fields injection_template_updatevo: 注入UpdateVO模板
	 */
	protected String injection_template_updatevo = "templates/updateVO.java";

	/**
	 * @Fields injection_template_vo: 注入VO模板
	 */
	protected String injection_template_vo = "templates/vo.java";

	/**
	 * @Fields injection_template_pagevo: 注入PageVO模板
	 */
	protected String injection_template_pagevo = "templates/pageVO.java";

	/**
	 * @Fields injection_template_briefvo: 注入BriefVO模板
	 */
	protected String injection_template_briefvo = "templates/briefVO.java";

	/**
	 * @Fields injection_template_treevo: 注入TreeVO模板
	 */
	protected String injection_template_treevo = "templates/treeVO.java";

	/**
	 * @Fields injection_template_query: 注入Query模板
	 */
	protected String injection_template_query = "templates/query.java";

	/**
	 * @Fields injection_template_pagequery: 注入PageQuery模板
	 */
	protected String injection_template_pagequery = "templates/pageQuery.java";

	/**
	 * @Fields injection_template_briefquery: 注入BriefQuery模板
	 */
	protected String injection_template_briefquery = "templates/briefQuery.java";

	/**
	 * @Fields injection_template_lazyquery: 注入TreeLazyQuery模板
	 */
	protected String injection_template_lazyquery = "templates/lazyQuery.java";

	/**
	 * @Fields injection_template_treequery: 注入TreeQuery模板
	 */
	protected String injection_template_treequery = "templates/treeQuery.java";

	/**
	 * @Fields injection_template_complexquery: 注入TreeComplexQuery模板
	 */
	protected String injection_template_complexquery = "templates/complexQuery.java";

	/**
	 * @Fields injection_template_responsecode: 注入ResponseCode模板
	 */
	protected String injection_template_responsecode = "templates/responseCode.java";

	/**
	 * @Fields injection_template_converter: 注入Converter模板
	 */
	protected String injection_template_converter = "templates/converter.java";

	/**
	 * @Fields injection_template_proc_update_outline: 注入proc_update_outline模板
	 */
	protected String injection_template_proc_update_outline = "templates/sql/proc_update_outline.sql";

	/**
	 * @Fields injection_template_proc_parent_nodes: 注入proc_parent_nodes模板
	 */
	protected String injection_template_proc_parent_nodes = "templates/sql/proc_parent_nodes.sql";

	/**
	 * @Fields injection_template_proc_children_nodes: 注入proc_children_nodes模板
	 */
	protected String injection_template_proc_children_nodes = "templates/sql/proc_children_nodes.sql";

	/**
	 * @Fields injection_template_proc_leaf_nodes: 注入proc_leaf_nodes模板
	 */
	protected String injection_template_proc_leaf_nodes = "templates/sql/proc_leaf_nodes.sql";

	/**
	 * @Fields injection_template_proc_full_nodes: 注入proc_full_nodes模板
	 */
	protected String injection_template_proc_full_nodes = "templates/sql/proc_full_nodes.sql";
}
