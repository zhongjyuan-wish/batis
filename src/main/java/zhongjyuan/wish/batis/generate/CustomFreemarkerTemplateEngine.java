package zhongjyuan.wish.batis.generate;

import java.io.File;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.config.ConstVal;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import zhongjyuan.domain.Constant;

/**
 * @ClassName CustomFreemarkerTemplateEngine
 * @Description 自定义Freemarker模板引擎对象
 * @Author zhongjyuan
 * @Date 2023年1月5日 下午2:44:31
 * @Copyright zhongjyuan.com
 */
public class CustomFreemarkerTemplateEngine extends FreemarkerTemplateEngine {

	/**
	 * @Title outputCustomFile
	 * @Description 输出自定义文件
	 * @param customFile 自定义文件集合
	 * @param tableInfo  表信息对象
	 * @param objectMap  配置Map集
	 * @see com.baomidou.mybatisplus.generator.engine.AbstractTemplateEngine#outputCustomFile(java.util.Map,
	 *      com.baomidou.mybatisplus.generator.config.po.TableInfo, java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	protected void outputCustomFile(@NotNull Map<String, String> customFile, @NotNull TableInfo tableInfo, @NotNull Map<String, Object> objectMap) {

		Boolean codeFlag = tableInfo.getFieldNames().contains(", " + Constant.CODE_L);
		Boolean nameFlag = tableInfo.getFieldNames().contains(", " + Constant.NAME_L);
		Boolean typeFlag = tableInfo.getFieldNames().contains(", " + Constant.TYPE_L);
		Boolean groupFlag = tableInfo.getFieldNames().contains(", " + Constant.GROUP_L);
		Boolean treeFlag = tableInfo.getFieldNames().contains(Constant.PARENT_ID_HUMP_L_A);
		Boolean lockFlag = tableInfo.getFieldNames().contains(Constant.IS_LOCK_HUMP_L_A);
		Boolean systemFlag = tableInfo.getFieldNames().contains(Constant.IS_SYSTEM_HUMP_L_A);
		Boolean enabledFlag = tableInfo.getFieldNames().contains(Constant.IS_ENABLED_HUMP_L_A);
		Boolean deletedFlag = tableInfo.getFieldNames().contains(Constant.IS_DELETED_HUMP_L_A);
		String entityNaming = getConfigBuilder().getStrategyConfig().entity().getNameConvert().entityNameConvert(tableInfo);
		String keyPropertyType = tableInfo.getFields().stream().filter(field -> field.isKeyFlag()).findFirst().get().getPropertyType();

		objectMap.put("codeFlag", codeFlag);
		objectMap.put("nameFlag", nameFlag);
		objectMap.put("typeFlag", typeFlag);
		objectMap.put("groupFlag", groupFlag);
		objectMap.put("treeFlag", treeFlag);
		objectMap.put("lockFlag", lockFlag);
		objectMap.put("systemFlag", systemFlag);
		objectMap.put("enableFlag", enabledFlag);
		objectMap.put("deletedFlag", deletedFlag);
		objectMap.put("entityNaming", entityNaming);
		objectMap.put("keyPropertyType", keyPropertyType);

		String outputDir = getConfigBuilder().getGlobalConfig().getOutputDir();
		Map<String, Object> packageMap = (Map<String, Object>) objectMap.get("package");
		String packageParent = packageMap.get(ConstVal.PARENT).toString();

		customFile.forEach((key, value) -> {

			String packagePath = null;

			if (key.endsWith("VO") || key.endsWith("PageVO") || key.endsWith("BriefVO") || key.endsWith("TreeVO")) {
				packagePath = objectMap.get("package_ResponseVO").toString();
			}

			if (key.endsWith("CreateVO") || key.endsWith("UpdateVO")) {
				packagePath = objectMap.get("package_RequestVO").toString();
			}

			if (key.endsWith("Query")) {
				packagePath = objectMap.get("package_Query").toString();
			}

			if (key.endsWith("Code")) {
				packagePath = objectMap.get("package_Model").toString();
			}

			if (key.endsWith("Converter")) {
				packagePath = objectMap.get("package_Converter").toString();
			}

			if (!treeFlag && (key.endsWith("TreeVO") || key.endsWith("LazyQuery") || key.endsWith("TreeQuery") || key.endsWith("ComplexQuery"))) {
				return;
			}

			String fileName = "";
			if (key.endsWith("update_outline.sql") || key.endsWith("parent_nodes.sql") || key.endsWith("children_nodes.sql") || key.endsWith("leaf_nodes.sql") || key.endsWith("full_nodes.sql")) {
				if (!treeFlag) {
					return;
				} else {
					fileName = System.getProperty("user.dir") + "/src/main/resources/db/deploy/001.schema/procedure" + String.format(File.separator + key, tableInfo.getName());
				}
			} else {
				fileName = joinPath(outputDir, String.format((packageParent + File.separator + packagePath + File.separator + key), entityNaming)) + suffixJavaOrKt();
			}
			outputFile(new File(fileName), objectMap, templateFilePath(value), getConfigBuilder().getInjectionConfig().isFileOverride());
		});
	}

	/**
	 * @Title joinPath
	 * @Author zhongjyuan
	 * @Description 拼接路径
	 * @Param @param parentDir 父级目录
	 * @Param @param packageName 包名
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	private String joinPath(String parentDir, String packageName) {
		if (StringUtils.isBlank(parentDir)) {
			parentDir = System.getProperty(ConstVal.JAVA_TMPDIR);
		}
		if (!StringUtils.endsWith(parentDir, File.separator)) {
			parentDir += File.separator;
		}
		packageName = packageName.replaceAll("\\.", StringPool.BACK_SLASH + File.separator);
		return parentDir + packageName;
	}
}
