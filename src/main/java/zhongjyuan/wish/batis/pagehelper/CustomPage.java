package zhongjyuan.wish.batis.pagehelper;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.query.PageQuery;
import zhongjyuan.wish.batis.utils.PageUtil;

/**
 * @ClassName CustomPage
 * @Description 自定义分页对象
 * @Author zhongjyuan
 * @Date 2023年1月4日 上午10:24:12
 * @param <T>
 * @Copyright zhongjyuan.com
 */
public class CustomPage<T> extends Page<T> {

	private static final long serialVersionUID = 1L;

	/**
	 * @Title CustomPage
	 * @Author zhongjyuan
	 * @Description 自定义分页对象
	 * @Param @param query 分页查询条件对象
	 * @Param @param defaultSortName 默认排序名称: 默认RowIndex
	 * @Throws
	 */
	public CustomPage(PageQuery query, String defaultSortName) {

		// 父级构造函数
		super(ObjectUtils.isNull(query.getPageIndex()) ? 1 : query.getPageIndex(), ObjectUtils.isNull(query.getPageSize()) ? 20 : query.getPageSize());

		// 排序名称处理
		String sortName = query.getSortName();
		if (StringUtils.isBlank(sortName))
			sortName = StringUtils.isBlank(defaultSortName) ? Constant.ROW_INDEX_L_A : defaultSortName;

		// 排序类型处理
		String sortType = query.getSortType();
		if (StringUtils.isBlank(sortType))
			sortType = Constant.ASC_SORT_TYPE;

		// 排序线构造
		OrderItem orderItem = null;
		switch (sortType) {
			case Constant.ASC_SORT_TYPE:
			case Constant.ASC_SORT_TYPE_INT:
			case Constant.ASCENDING_SORT_TYPE:
				orderItem = OrderItem.asc(PageUtil.camelToUnderline(sortName));
				break;
			case Constant.DESC_SORT_TYPE:
			case Constant.DESC_SORT_TYPE_INT:
			case Constant.DESCENDING_SORT_TYPE:
				orderItem = OrderItem.desc(PageUtil.camelToUnderline(sortName));
				break;
			default:
				orderItem = OrderItem.asc(PageUtil.camelToUnderline(sortName));
				break;
		}

		// 增加分页排序项
		if (orderItem != null) {
			super.addOrder(orderItem);
		}
	}
}
