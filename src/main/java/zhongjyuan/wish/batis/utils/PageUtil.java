package zhongjyuan.wish.batis.utils;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import org.apache.commons.collections4.CollectionUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import zhongjyuan.domain.AbstractEmptyEntity;
import zhongjyuan.domain.AbstractModel;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.query.SortQuery;
import zhongjyuan.domain.response.PageResult;

/**
 * @ClassName PageUtil
 * @Description 分页工具
 * @Author zhongjyuan
 * @Date 2023年1月4日 上午10:59:37
 * @Copyright zhongjyuan.com
 */
public class PageUtil {

	/**
	 * @Title convert
	 * @Author zhongjyuan
	 * @Description 转换
	 * @Param @param <VO> VO泛型对象
	 * @Param @param list VO对象集合
	 * @Param @return
	 * @Return PageResult<VO> 分页结果对象
	 * @Throws
	 */
	public static <VO extends AbstractModel> PageResult<VO> convert(List<VO> list) {
		return new PageResult<VO>(list, Long.valueOf(list.size()), 20L, 1L);
	}

	/**
	 * @Title convert
	 * @Author zhongjyuan
	 * @Description 转换
	 * @Param @param <Entity> 实体泛型对象
	 * @Param @param page 分页接口
	 * @Param @return
	 * @Return PageResult<Entity> 分页结果对象
	 * @Throws
	 */
	public static <Entity extends AbstractModel> PageResult<Entity> convert(IPage<Entity> page) {
		PageResult<Entity> result = new PageResult<Entity>();

		result.withList(page.getRecords());
		result.withIndex((Long) page.getCurrent());
		result.withSize((Long) page.getSize());
		result.withTotal(page.getTotal());
		result.withPages((Long) page.getPages());

		return result;
	}

	/**
	 * @Title convert
	 * @Author zhongjyuan
	 * @Description 转换
	 * @Param @param <Entity> 实体泛型对象
	 * @Param @param <VO> VO泛型对象
	 * @Param @param page 分页接口
	 * @Param @param list VO对象集合
	 * @Param @return
	 * @Return PageResult<VO> 分页结果对象
	 * @Throws
	 */
	public static <Entity extends AbstractEmptyEntity, VO extends AbstractModel> PageResult<VO> convert(IPage<Entity> page, List<VO> list) {
		PageResult<VO> myPageBean = new PageResult<VO>();

		myPageBean.withList(list);
		myPageBean.withIndex((Long) page.getCurrent());
		myPageBean.withSize((Long) page.getSize());
		myPageBean.withTotal(page.getTotal());
		myPageBean.withPages((Long) page.getPages());

		return myPageBean;
	}

	/**
	 * @Title convert
	 * @Author zhongjyuan
	 * @Description 转换
	 * @Param @param <Entity> 实体泛型对象
	 * @Param @param <VO> VO泛型对象
	 * @Param @param page 分页接口
	 * @Param @param converter 转换器函数
	 * @Param @return
	 * @Return PageResult<VO> 分页结果对象
	 * @Throws
	 */
	public static <Entity extends AbstractEmptyEntity, VO extends AbstractModel> PageResult<VO> convert(IPage<Entity> page, Function<List<Entity>, List<VO>> converter) {
		PageResult<VO> myPageBean = new PageResult<VO>();

		if (CollectionUtils.isEmpty(page.getRecords()))
			return myPageBean;

		myPageBean.withList(converter.apply(page.getRecords()));
		myPageBean.withIndex((Long) page.getCurrent());
		myPageBean.withSize((Long) page.getSize());
		myPageBean.withTotal(page.getTotal());
		myPageBean.withPages((Long) page.getPages());

		return myPageBean;
	}

	/**
	 * @Title wrapperSort
	 * @Author zhongjyuan
	 * @Description 包装排序
	 * @Param @param queryWrapper 查询包装对象
	 * @Param @param query 查询条件对象
	 * @Return void
	 * @Throws
	 */
	public static <Entity extends AbstractEmptyEntity> void wrapperSort(QueryWrapper<Entity> queryWrapper, SortQuery query) {
		// 排序类型处理
		String sortType = query.getSortType();
		if (StringUtils.isBlank(sortType))
			sortType = Constant.ASC_SORT_TYPE;

		switch (sortType) {
			case Constant.ASC_SORT_TYPE:
			case Constant.ASC_SORT_TYPE_INT:
			case Constant.ASCENDING_SORT_TYPE:
				if (StringUtils.isNotBlank(query.getSortName())) {
					queryWrapper.orderByAsc(Arrays.asList(camelToUnderline(query.getSortName())));
				} else {
					queryWrapper.orderByAsc(Arrays.asList(Constant.ROW_INDEX_HUMP_L_A));
				}
				break;
			case Constant.DESC_SORT_TYPE:
			case Constant.DESC_SORT_TYPE_INT:
			case Constant.DESCENDING_SORT_TYPE:
				if (StringUtils.isNotBlank(query.getSortName())) {
					queryWrapper.orderByDesc(Arrays.asList(camelToUnderline(query.getSortName())));
				} else {
					queryWrapper.orderByDesc(Arrays.asList(Constant.ROW_INDEX_HUMP_L_A));
				}
				break;
			default:
				queryWrapper.orderByAsc(Arrays.asList(Constant.ROW_INDEX_HUMP_L_A));
				break;
		}
	}

	/**
	 * @Title camelToUnderline
	 * @Author zhongjyuan
	 * @Description 驼峰处理
	 * @Param @param param 参数
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String camelToUnderline(String param) {
		return com.baomidou.mybatisplus.core.toolkit.StringUtils.camelToUnderline(param);
	}
}
